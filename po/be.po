# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Alexander Nyakhaychyk <nyakhaychyk@gmail.com>, 2007
msgid ""
msgstr ""
"Project-Id-Version: Thunar Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-07-03 20:07+0200\n"
"PO-Revision-Date: 2017-09-19 18:03+0000\n"
"Last-Translator: Nick Schermer <nick@xfce.org>\n"
"Language-Team: Belarusian (http://www.transifex.com/xfce/thunar-plugins/language/be/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: be\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#. prepare the dialog to query the preferred archiver for the user
#: ../thunar-archive-plugin/tap-backend.c:81
msgid "Select an archive manager"
msgstr "Вылучыце кіраўніка архіваў"

#. add the header label
#: ../thunar-archive-plugin/tap-backend.c:105
msgid ""
"Please select your preferred archive manager\n"
"from the list of available applications below:"
msgstr "Калі ласка, вылучыце пераважны кіраўнік\nархіваў з гэтага сьпісу дастасаваньняў:"

#. tell the user that we cannot handle the specified mime types
#: ../thunar-archive-plugin/tap-backend.c:280
#: ../thunar-archive-plugin/tap-backend.c:427
#, c-format
msgid "No suitable archive manager found"
msgstr "Ня знойдзены неабходны кіраўнік архіваў"

#. execute the action
#: ../thunar-archive-plugin/tap-provider.c:336
#: ../thunar-archive-plugin/tap-provider.c:375
msgid "Failed to extract files"
msgstr "Немагчыма распакаваць файлы"

#. execute the action
#: ../thunar-archive-plugin/tap-provider.c:413
msgid "Failed to create archive"
msgstr "Немагчыма стварыць архіў"

#: ../thunar-archive-plugin/tap-provider.c:468
msgid "Extract _Here"
msgstr "Распакаваць с_юды"

#: ../thunar-archive-plugin/tap-provider.c:475
msgid "Extract the selected archive in the current folder"
msgid_plural "Extract the selected archives in the current folder"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: ../thunar-archive-plugin/tap-provider.c:492
msgid "_Extract To..."
msgstr "Рас_пакаваць у..."

#: ../thunar-archive-plugin/tap-provider.c:500
msgid "Extract the selected archive"
msgid_plural "Extract the selected archives"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#: ../thunar-archive-plugin/tap-provider.c:520
msgid "Cr_eate Archive..."
msgstr "С_тварыць архіў..."

#: ../thunar-archive-plugin/tap-provider.c:528
msgid "Create an archive with the selected object"
msgid_plural "Create an archive with the selected objects"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

#. TRANSLATORS: This is the label of the Drag'n'Drop "Extract here" action
#: ../thunar-archive-plugin/tap-provider.c:596
msgid "_Extract here"
msgstr "Распакаваць с_юды"

#: ../thunar-archive-plugin/tap-provider.c:603
msgid "Extract the selected archive here"
msgid_plural "Extract the selected archives here"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
